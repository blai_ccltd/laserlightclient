/*****************************************************************
* Module Name: Web Form Tab Page
* Description: To display content of in Tab Pages
* Author: blai
* Date Modified: 2017.09.07
* Related files: wTab.html
*****************************************************************/

app.controller('wTabCtrl', function($scope, $rootScope, $state, $stateParams,  $ionicPopup, $window, $ionicHistory, $q, $timeout, $mdSidenav, $mdToast, $filter) {

  $scope.currentNavItem = 0;
  $rootScope.navItems = [];
  $rootScope.addNavPage = function(page){
    var exists = false;
    var found = -1;
    var index = 0;

    $rootScope.navItems.forEach(function(item){
      if(item.name === page.name){
        exists = true;
        found = index;
      }
      index++;
    });
    if(!exists){
      $rootScope.navItems.push(page);
      $scope.currentNavItem = $rootScope.navItems.length - 1;
      //$state.go(state, param);
    }else{
      $scope.currentNavItem = found;
      if(page.param){
        $state.go(page.state, page.param);
      }else{
        $state.go(page.state);
      }
    }
  };

  $rootScope.changeNavPage = function(page){
    $rootScope.navItems[$scope.currentNavItem] = page;
    $state.go(page.state, page.param);
  };

  $rootScope.closePage = function(){
    $rootScope.navItems.splice($scope.currentNavItem,1);
    $scope.currentNavItem = $rootScope.navItems.length-1;
    if($rootScope.navItems[$rootScope.navItems.length-1].param){
      $state.go($rootScope.navItems[$rootScope.navItems.length-1].state, $rootScope.navItems[$rootScope.navItems.length-1].param);
    }else{
      $state.go($rootScope.navItems[$rootScope.navItems.length-1].state);
    }
  }

  $rootScope.changeMenu = function(menu){
    $rootScope.navItems[0] = menu;
    $scope.currentNavItem = 0;
    $state.go(menu.state);
  }

  $scope.load = function() {
    $rootScope.navItems.push({statePath:"lasersensor.tab.main", name:'main', title:$filter('translate')('ALLSENSORS'), state:"lasersensor.tab.main"});
    $state.go("lasersensor.tab.main")
  };/*End of Load Function*/
});
