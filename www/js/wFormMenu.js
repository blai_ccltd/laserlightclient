/*****************************************************************
* Module Name: Web Form Main Menu
* Description: To display menu, header and navigation link
* Author: dredmonds
* Date Modified: 2016.09.23
* Related files: wFormMenu.html
*****************************************************************/

app.controller('wFormMenuCtrl', function($scope, $rootScope, $filter, $state, $ionicHistory, $ionicPopup, $window, $mdEditDialog, $q, $timeout, $mdSidenav, $mdToast, $translate, httpService) {

  $scope.toggleSidenav = function(menu) {
    $mdSidenav(menu).toggle();
  }

  $scope.toast = function(message) {
    var toast = $mdToast.simple().content('You clicked ' + message).position('bottom right');
    $mdToast.show(toast);

    if(message === 'click-all'){
      $rootScope.changeMenu({statePath:"lasersensor.tab.main", name:'main', title:$filter('translate')('ALLSENSORS'), state:"lasersensor.tab.main"});
    }

    if ($mdSidenav("left").isOpen()) {
      $scope.toggleSidenav("left", true);
    }
  };

  $scope.toastList = function(message) {
    var toast = $mdToast.simple().content('You clicked ' + message + ' having selected ' + $scope.selected.length + ' item(s)').position('bottom right');
    $mdToast.show(toast);
  };
  //TODO:Translate 401s

  $scope.load = function(){

    $scope.reloadMenu();

  }/*End of load function*/

  $scope.reloadMenu = function(){

    /******* Menu Data *******/
    $scope.wFormMenuData = {};

    $scope.wFormMenuData.title = $filter('translate')('CCTITLE');

    $scope.wFormMenuData.sidenav = {};

    $scope.wFormMenuData.sidenav.menus = [];
    $scope.wFormMenuData.sidenav.sections = [];
    var menuSection1 = {};
    menuSection1.name = $filter('translate')('FUNCTIONS');
    menuSection1.expand = true;
    menuSection1.actions = [];

    var menuSection1Action1 = {};
    menuSection1Action1.name = $filter('translate')('ALLSENSORS');
    menuSection1Action1.icon = 'mdi-message-text';
    menuSection1Action1.link = 'click-all';
    menuSection1.actions.push(menuSection1Action1);

    $scope.wFormMenuData.sidenav.sections.push(menuSection1);
  }

});
