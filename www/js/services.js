app.factory('Dummy', function() {
  //var abc = "";
  return {
    a: function() {
      return "";
    }
  };
});

app.service('invoiceStagingService', function(){
  var pOrderData = {};
  var sInvoiceData = {};

  var setOrder  = function(orderData){
    pOrderData = orderData;
  }

  var getOrder = function(){
    return pOrderData;
  }

  var setInvoice = function(invoiceData){
    sInvoiceData = invoiceData;
  }

  var getInvoice = function(){
    return sInvoiceData;
  }

  return {
    setOrder: setOrder,
    getOrder: getOrder,
    setInvoice: setInvoice,
    getInvoice: getInvoice
  };
});

app.service('lineItemsStagingService', function(){
  var msgIDs = [];
  var msgType = "";
  var lineItems = [];

  var setMsgIDs = function(msgIDList){
    msgIDs = msgIDList;
  }

  var getMsgIDs = function(){
    return msgIDs;

  }

  var setMsgType = function(msgTypeString){
    msgType = msgTypeString;
  }

  var getMsgType = function(){
    return msgType;
  }

  var setLineItemData = function(lineItemList){
    lineItems = lineItemList;
  }

  var getLineItemData = function(){
    return lineItems;
  }

  return {
    setMsgIDs: setMsgIDs,
    getMsgIDs: getMsgIDs,
    setMsgType: setMsgType,
    getMsgType: getMsgType,
    setLineItemData: setLineItemData,
    getLineItemData: getLineItemData
  };
});

app.service('copyDocumentService', ['$rootScope', '$filter', function($rootScope, $filter){
  let documentData = {};

  let openDocument = function(){
    switch (documentData.msgType){
      case 'ORDER':
        $rootScope.addNavPage({ statePath:"webForm.tab.orderEdit({msgID:''" + ", isCopy: true})",
                                name:"orderEdit" + documentData.orderNumber,
                                title:$filter('translate')('EDITORDER')+ " " + documentData.orderNumber + " (copy)",
                                state:'webForm.tab.orderEdit',
                                param:{"msgID": "", 'isCopy': true}});
        break;

      case 'INVOICE':
        $rootScope.addNavPage({ statePath: "webForm.tab.invoiceEdit({action:'invoice', isCopy: true})",
                                name:"invoiceEdit" + documentData.invoiceNumber,
                                title:$filter('translate')('EDITINVOICE')+ " " + documentData.invoiceNumber + " (copy)",
                                state:'webForm.tab.invoiceEdit',
                                param:{action:"invoice", 'isCopy': true}});
        break;
      case 'ASN':
        $rootScope.addNavPage({ statePath:"webForm.tab.shippingNoteEdit({msgID:''" + ", isCopy: true})",
                                name:'shippingNoteEdit' + documentData.documentNumber,
                                title:"Edit Shipping Note " + documentData.documentNumber,
                                state:'webForm.tab.shippingNoteEdit',
                                param:{'msgID': "", 'isCopy': true}});
        break;
      case 'ORDERACK':
        // not implemented
        break;
    }
  }

  let setDocument = function(documentObj){
    documentData = documentObj;
    openDocument();
  }

  let getDocument = function(){
    return documentData;
  }

  return {
    setDocument: setDocument,
    getDocument: getDocument
  };
}])

/*create product from line item import*/
app.service('createProductService', function(){
  var productData = {};

  var setProductData = function(productDataObj){
    productData = productDataObj;
  }

  var getProductData = function(){
    return productData;
  }

  return {
    setProductData: setProductData,
    getProductData: getProductData
  };
})

app.service('dialogService', ['$ionicPopup', '$q', '$filter', function($ionicPopup, $q, $filter){
  // confirm dialog
  var showConfirm = function(options, buttonText) {
    let defer = $q.defer();

    // default options
    if (!options.buttons){
      options.buttons = [{
        text: ($filter('translate')(buttonText && buttonText.positive ? buttonText.positive : 'OK')),
        type: 'button-positive',
        onTap: function() {
          return true;
        }
      },
      {
        text: ($filter('translate')(buttonText && buttonText.negative ? buttonText.negative : 'CANCEL')),
        type: 'button-default',
        onTap: function() {
          return false;
        }
      }]
    }

    let confirmPopup = $ionicPopup.confirm(options);

    confirmPopup.then(function(res) {
      defer.resolve(res)
    });
    return defer.promise;
  };

  // alert dialog
  var showAlert = function(options, buttonText) {
    let defer = $q.defer();

    // default options
    if (!options.okText){
      options.okText = ($filter('translate')((buttonText && buttonText.positive) ? buttonText.positive : 'OK'));
    }
    if (!options.okType) options.okType = 'button-positive';

    let alertPopup = $ionicPopup.alert(options);

    alertPopup.then(function(res) {
      defer.resolve(res)
    });
    return defer.promise;
  };

   return {
    showConfirm: showConfirm,
    showAlert: showAlert
   }
}])

app.service('httpService', ['$rootScope', '$http', '$q', '$filter', 'dialogService', function($rootScope, $http, $q, $filter,dialogService){
  var onError = function(status, result){
    if(status === 419){ // error code 419, Authentication Timeout
      dialogService.showAlert({
        title: ($filter('translate')('TIMEDOUTTITLE')),
        template: ($filter('translate')('TIMEDOUTTEXT'))
      }).then(function(res){
        $state.go('index');
      });
      return;
    } else if (status === 401){ // error code 401, unauthorized. force login
      dialogService.showAlert({
        title: ($filter('translate')('UNAUTHORIZEDTITLE')),
        template: ($filter('translate')('UNAUTHORIZEDTEXT'))
      }).then(function(res){
        window.localStorage.removeItem("accessToken");
        $state.go('index');
      });
    } else {
      if (status !== -1 && result.error){
        dialogService.showAlert({
          title: ($filter('translate')(result.error.title)),
          template: ($filter('translate')(result.error.template))
        }).then(function(res){
          $rootScope.regLoad();
        });
      } else {
        dialogService.showAlert({
          title: ($filter('translate')('UNKNOWNERRORTITLE')),
          template: ($filter('translate')('STATUSCODE') + ": " + status)
        }).then(function(res){
          $rootScope.regLoad();
        });
      }
    }
  }

  var postReq = function(url, req){
    console.log("postReq", "url:", url, "req:", req);

    let defer = $q.defer();
    req.accessToken = $rootScope.accessToken; // prepares accessToken

    $http.post( $rootScope.baseURL + url, req,
                {headers: {'Content-Type': 'application/json'}})
    .then(function(data, status, headers, config){
      let result = data.data;
      result.success = true;
      defer.resolve(result); // resolves promise
    }, function(data, status, headers, config){ // if request fails
      let result = {};
      result.success = false;
      defer.resolve(result); // resolves promise

      //onError(data.status, result);
    });
    return defer.promise; // returns promise
  }

  var putReq = function(url, req){
    console.log("putReq", "url:", url, "req:", req);

    let defer = $q.defer();
    req.accessToken = $rootScope.accessToken; // prepares accessToken

    $http.put( $rootScope.baseURL + url, req,
                {headers: {'Content-Type': 'application/json'}})
    .then(function(data, status, headers, config){
      let result = data.data;
      result.success = true;
      defer.resolve(result); // resolves promise
    }, function(data, status, headers, config){ // if request fails
      let result = {};
      result.success = false;
      defer.resolve(result); // resolves promise

      onError(data.status, result);
    });
    return defer.promise; // returns promise
  }

  var getReq = function(url, req){
    console.log("getReq", "url:", url, "req:", req);

    let defer = $q.defer();
    /*creates string to pass parameters in url*/
    let paramsString = "";
    if (req){
      req.forEach(function(param){
        paramsString += ('&' + param.name + '=' + param.value);
      })
    }

    $http.get($rootScope.baseURL + url + "?accessToken=" + $rootScope.accessToken + paramsString, req,
              {headers: {'Content-Type': 'application/json'}})
    .then(function(data, status, headers, config){
      let result = data.data;
      console.log(result);
      result.success = true;
      defer.resolve(result); // resolves promise
    }, function(data, status, headers, config){ // if request fails
      let result = {};
      result.success = false;
      defer.resolve(result); // resolves promise

      onError(data.status, result);
    });
    return defer.promise; // returns promise
  }

  return {
    postReq: postReq,
    putReq: putReq,
    getReq: getReq
  };
}])

app.service('documentHistoryService', ['$filter', 'httpService', function($filter, httpService){
  let pushHistory = function(msgIDList, msgType, status, description){
    /*create history object*/
    var historyObj = {
          msgIDList       : msgIDList,
          revision    : {revisedBy: JSON.parse(window.localStorage.getItem("user")).loginID,
                         revisedDate: new Date($filter('date')(new Date(),"yyyy-MM-dd HH:mm:ss")), //how to change filter for miliseconds?
                         revisedStatus: status,
                         revisedDesc: description}
    };

    /*set correct request for message type*/
    switch(msgType){
      case 'ORDER':
        var url = "/orders/history/";
        break;
      case 'INVOICE':
        var url = "/invoices/history/";
        break;
      case 'SHIPPINGNOTE':
        var url = "/shippingnote/history/";
        break;
    }

    httpService.postReq(url, historyObj).then(function(result){
      console.log(result.result);
      if (result.success){
        console.log("Success, " + msgType + " document history appended.");
      } else {

console.log("result", result);
        console.log("Error, " + msgType + " document history not appended.");
      }
    })

    // /*post revision to server*/
    // $http.post($scope.baseURL + url, historyObj, {headers: {'Content-Type': 'application/json'}})
    // .then(function(data, status, headers, config){
    //   //success
    //   console.log("Success, " + msgType + " document history appended.");
    // }, function (data, status, headers, config) {
    //   //Error
    //   console.log("Error, " + msgType + " document history not appended.");
    // });
    return historyObj.revision;
  }
  return {
    pushHistory: pushHistory
  };
}])

app.service('vatBreakdownService', ['$rootScope', function($rootScope){
  let calculateBreakdown = function(lineItems, surcharges, surchargesVATCode){
    let breakdown = {};
    for (i in lineItems){
      let vatCode = lineItems[i].itemVATCode || $rootScope.settings.defaultVAT;

      // if VAT code does not exist, create it //
      if (!breakdown[vatCode]) {
        breakdown[vatCode] = {percent: Number(vatCode.split('-')[1]) * 0.01,
                              lines: 0,
                              lineTotal: 0,
                              surcharges: 0,
                              taxableAmount: 0,
                              VATAmount: 0,
                              total:0};

      if (vatCode == surchargesVATCode){breakdown[vatCode].surcharges = Number(surcharges)}
      }
      breakdown[vatCode].lines += 1;
      breakdown[vatCode].lineTotal += lineItems[i].itemPrice * lineItems[i].quantity;
    }
    // if VAT code for surcharges does not exist, create it //
    if (!(surchargesVATCode in breakdown) && surcharges){
      breakdown[surchargesVATCode] = {percent: Number(surchargesVATCode.split('-')[1]) * 0.01,
                                      lines: 0,
                                      lineTotal: 0,
                                      surcharges: Number(surcharges),
                                      taxableAmount: 0,
                                      VATAmount: 0,
                                      total:0};
    }

    // create totals //
    let totalLineTotal = 0;
    let totalSurcharges = 0;
    let totalVATAmount = 0;
    let totalInvoice = 0;

    for (vatCode in breakdown) {
      breakdown[vatCode].lineTotal = roundUp(breakdown[vatCode].lineTotal);
      breakdown[vatCode].taxableAmount = breakdown[vatCode].lineTotal + breakdown[vatCode].surcharges;
      breakdown[vatCode].VATAmount = roundUp(breakdown[vatCode].taxableAmount * breakdown[vatCode].percent);
      breakdown[vatCode].total = breakdown[vatCode].taxableAmount + breakdown[vatCode].VATAmount;

      totalLineTotal += breakdown[vatCode].lineTotal;
      totalSurcharges += breakdown[vatCode].surcharges;
      totalVATAmount += breakdown[vatCode].VATAmount;
      totalInvoice += breakdown[vatCode].total;
    }

    let totalTaxableAmount = totalLineTotal + totalSurcharges;

    return {breakdown: breakdown,
            totalLineTotal: totalLineTotal,
            totalSurcharges: totalSurcharges,
            totalTaxableAmount: totalTaxableAmount,
            totalVATAmount: totalVATAmount,
            totalInvoice: totalInvoice};
  }

  return {
    calculateBreakdown: calculateBreakdown
  }
}])

app.service('counterService', ['httpService', function(httpService){
  /*used to get any counter (FGN, SSCC)*/
  /*Must pass in: "function(output){ $scope.counter = output; }" as the second parameter to retrieve the value*/
  let getCounter = function(counter, callback){
    let getCounterList = [
      {name: 'companyID', value: JSON.parse(window.localStorage.getItem('company')).companyID},
      {name: 'counter', value: counter}
    ]
    // let companyID = JSON.parse(window.localStorage.getItem('company')).companyID;

    httpService.getReq("/systemprofile/getcounter", getCounterList).then(function(result){
      callback(result.value);
      console.log("Retrieving counter: " + counter + ". Response: ", result);
    })
  }

  /*used to set any counter (FGN, SSCC) negative values can be used*/
  let setCounter = function(counter, newValue){
    setCounterObj = {
      companyID: JSON.parse(window.localStorage.getItem('company')).companyID,
      counter: counter,
      newValue: newValue
    }

    httpService.putReq("/systemprofile/setcounter", setCounterObj).then(function(result){
      console.log("Setting counter: " + setCounterObj.counter + ". New value: " + setCounterObj.newValue + ". Response: ", result);
    })
  }

  /*used to increment any counter (FGN, SSCC) negative values can be used*/
  let incrementCounter = function(counter, incrementBy){
    let incrememntCounterObj = {
      companyID: JSON.parse(window.localStorage.getItem('company')).companyID,
      counter: counter,
      incrementBy: incrementBy || 1
    }
    httpService.putReq("/systemprofile/incrementcounter", incrememntCounterObj).then(function(result){
      console.log("Incrementing counter: " + incrememntCounterObj.counter + ". Increment by: " + incrememntCounterObj.incrementBy + ". Response: ", result);
    })
  }

  return {
    getCounter: getCounter,
    setCounter: setCounter,
    incrementCounter: incrementCounter
  }
}])
