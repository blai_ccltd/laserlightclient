app.service('columnDefs',
function($filter){
	columnDefs = {
    orderDetails: [
      {
         field: 'lineNumber',
         type: 'number',
         displayName: $filter('translate')('LINENO'),
         headerCellClass: 'ui-grid-header',
         cellTemplate: '<span class="line-item-button"><i class="mdi mdi-dark mdi-18px mdi-plus-box" ng-if="row.entity.createProduct === \'1\'" ng-click="grid.appScope.addProduct(row.entity)"></i></span><span class="line-item-number">{{ row.entity.lineNumber }}</span>',
         maxWidth: 75,
      },
      {
        field: 'supplierTUEAN',
        type: 'number',
        displayName: $filter('translate')('SUPPLIERTUEAN'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'supplierItemCode',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERITEMCODE'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'buyerItemCode',
        type: 'string',
        displayName: $filter('translate')('BUYERITEMCODE'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'CUinTU',
        type: 'number',
        displayName: $filter('translate')('CUinTU'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'quantity',
        type: 'number',
        displayName: $filter('translate')('QUANTITY'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'itemPrice',
        type: 'number',
        displayName: $filter('translate')('ITEMPRICE'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'description',
        type: 'string',
        displayName: $filter('translate')('DESCRIPTION'),
        headerCellClass: 'ui-grid-header'
      }
    ],

    orderEdit: [
      {
         field: 'lineNumber',
         type: 'number',
         displayName: $filter('translate')('LINENO'),
         headerCellClass: 'ui-grid-header',
         cellTemplate: '<span class="line-item-button"><i class="mdi mdi-dark mdi-18px mdi-plus-box" ng-if="row.entity.createProduct === \'1\'" ng-click="grid.appScope.addProduct(row.entity)"></i></span><span class="line-item-number">{{ row.entity.lineNumber }}</span>',
         maxWidth: 75,
      },
      {
        field: 'supplierTUEAN',
        type: 'number',
        displayName: $filter('translate')('SUPPLIERTUEAN'),
        headerCellClass: 'ui-grid-header',
        enableCellEdit: true
      },
      {
        field: 'supplierItemCode',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERITEMCODE'),
        headerCellClass: 'ui-grid-header',
        enableCellEdit: true
      },
      {
        field: 'buyerItemCode',
        type: 'string',
        displayName: $filter('translate')('BUYERITEMCODE'),
        headerCellClass: 'ui-grid-header',
        enableCellEdit: true
      },
      {
        field: 'CUinTU',
        type: 'number',
        displayName: $filter('translate')('CUinTU'),
        headerCellClass: 'ui-grid-header',
        enableCellEdit: true
      },
      {
        field: 'quantity',
        type: 'number',
        displayName: $filter('translate')('QUANTITY'),
        headerCellClass: 'ui-grid-header',
        enableCellEdit: true
      },
      {
        field: 'itemPrice',
        type: 'number',
        displayName: $filter('translate')('ITEMPRICE'),
        headerCellClass: 'ui-grid-header',
        enableCellEdit: true
      },
      {
        field: 'description',
        type: 'string',
        displayName: $filter('translate')('DESCRIPTION'),
        headerCellClass: 'ui-grid-header',
        enableCellEdit: true
      }
    ],

    invoiceDetails: [
      {
        field: 'lineNumber',
        displayName: $filter('translate')('LINENO'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'supplierTUEAN',
        displayName: $filter('translate')('SUPPLIERTUEAN'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'supplierItemCode',
        displayName: $filter('translate')('SUPPLIERITEMCODE'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'buyerItemCode',
        displayName: $filter('translate')('BUYERITEMCODE'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'CUinTU',
        displayName: $filter('translate')('CUinTU'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'quantity',
        displayName: $filter('translate')('QUANTITY'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'itemPrice',
        displayName: $filter('translate')('ITEMPRICE'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'itemVATCode',
        displayName: $filter('translate')('ITEMVAT'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'description',
        displayName: $filter('translate')('DESCRIPTION'),
        headerCellClass: 'ui-grid-header'
      },
    ],

    invoiceEdit: [
      {
        field: 'lineNumber',
        displayName: $filter('translate')('LINENO'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'supplierTUEAN',
        displayName: $filter('translate')('SUPPLIERTUEAN'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'supplierItemCode',
        displayName: $filter('translate')('SUPPLIERITEMCODE'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'buyerItemCode',
        displayName: $filter('translate')('BUYERITEMCODE'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'CUinTU',
        displayName: $filter('translate')('CUinTU'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'quantity',
        displayName: $filter('translate')('QUANTITY'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'itemPrice',
        displayName: $filter('translate')('ITEMPRICE'),
        headerCellClass: 'ui-grid-header'
      },
      {
        field: 'itemVATCode',
        displayName: $filter('translate')('ITEMVAT'),
        headerCellClass: 'ui-grid-header',
                        editableCellTemplate: "ui-grid/dropdownEditor",
                        editDropdownOptionsArray: /*$scope.VATCodesListGrid*/[], // TODO: get these values
                        editDropdownIdLabel: 'code',
                        editDropdownValueLabel: 'code'
      },
      {
        field: 'description',
        displayName: $filter('translate')('DESCRIPTION'),
        headerCellClass: 'ui-grid-header'
      },
    ],

    lineItemStaging: [
      {
        field: 'orderNumber',
        type: 'string',
        displayName: $filter('translate')('ORDERNO'),
        grouping: { groupPriority: 0 },
        sort: { priority: 1,
        direction: 'asc' },
        editableCellTemplate: 'ui-grid/dropdownEditor',
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'deliveryGLN',
        type: 'string',
        displayName: $filter('translate')('DELIVERYGLN'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'lineNumber',
        type: 'string',
        displayName: $filter('translate')('LINENO'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'supplierTUEAN',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERTUEAN'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'supplierItemCode',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERITEMCODE'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'buyerItemCode',
        type: 'string',
        displayName: $filter('translate')('BUYERITEMCODE'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'quantity',
        type: 'number',
        displayName: $filter('translate')('QUANTITY'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: true,
        maxWidth: 90,
      },
      {
        field: 'description',
        type: 'string',
        displayName: $filter('translate')('DESCRIPTION'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      }
    ],

    shippingNoteDetailsLineItems: [
      {
        field: 'orderNumber',
        type: 'string',
        displayName: $filter('translate')('ORDERNO'),
        grouping: { groupPriority: 0 },
        sort: { priority: 1,
        direction: 'asc' },
        editableCellTemplate: 'ui-grid/dropdownEditor',
        headerCellClass: 'ui-grid-header',
      },
      {
        field: 'deliveryGLN',
        type: 'string',
        displayName: $filter('translate')('DELIVERYGLN'),
        headerCellClass: 'ui-grid-header',
      },
      {
        field: 'lineNumber',
        type: 'string',
        displayName: $filter('translate')('LINENO'),
        headerCellClass: 'ui-grid-header',
      },
      {
        field: 'supplierTUEAN',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERTUEAN'),
        headerCellClass: 'ui-grid-header',
      },
      {field: 'supplierItemCode',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERITEMCODE'),
        headerCellClass: 'ui-grid-header',
      },
      {
        field: 'buyerItemCode',
        type: 'string',
        displayName: $filter('translate')('BUYERITEMCODE'),
        headerCellClass: 'ui-grid-header',
      },
      {
        field: 'quantity',
        type: 'number',
        displayName: $filter('translate')('QUANTITY'),
        headerCellClass: 'ui-grid-header',
        maxWidth: 90,
      },
      {
        field: 'description',
        type: 'string',
        displayName: $filter('translate')('DESCRIPTION'),
        headerCellClass: 'ui-grid-header',
      }
    ],

    shippingNoteDetailsAsnItems: [
      {
        field: 'packNumber',
        type: 'string',
        displayName: $filter('translate')('PACK'),
        headerCellClass: 'ui-grid-header',
        cellTemplate: '<div ng-show="row.treeLevel == 0">{{ COL_FIELD CUSTOM_FILTERS }}</div>',
        maxWidth: 75,
        grouping: { groupPriority: 0 },
        sort: { priority: 0,
direction: 'asc' },
      },
      {
        field: 'sscc',
        type: 'string',
        displayName: $filter('translate')('SSCC'),
        headerCellClass: 'ui-grid-header',
        /*cell template flags sscc as 'dirty' when changed by the user to allow the original sscc to be used again*/
        cellTemplate: '<input readonly="true" ng-model="grid.appScope.ssccList\[row.treeNode.children[0].row.entity.packNumber - 1\].sscc" ng-show="row.treeLevel == 0" />',
        width: 155,
      },
      {
        field: 'packType',
        type: 'string',
        displayName: $filter('translate')('PACKTYPE'),
        headerCellClass: 'ui-grid-header',
        cellTemplate: '<div ng-show="row.treeLevel == 0">{{ grid.appScope.packTypeList\[row.treeNode.children[0].row.entity.packNumber - 1\] }}</div>',
        maxWidth: 100,
      },
      {
        field: 'quantity',
        type: 'number',
        displayName: $filter('translate')('QUANTITY'),
        headerCellClass: 'ui-grid-header',
        maxWidth: 90,
      },
      {
        field: 'supplierItemCode',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERITEMCODE'),
        headerCellClass: 'ui-grid-header',
      },
      {
        field: 'description',
        type: 'string',
        displayName: $filter('translate')('DESCRIPTION'),
        headerCellClass: 'ui-grid-header',
      }
    ],

		shippingNoteEditLineItems: [
      {
	      field: 'orderNumber',
	      type: 'string',
	      displayName: $filter('translate')('ORDERNO'),
	      grouping: { groupPriority: 0 },
	      sort: { priority: 1,
	      direction: 'asc' },
	      editableCellTemplate: 'ui-grid/dropdownEditor',
	      headerCellClass: 'ui-grid-header',
	      cellEditableCondition: false
      },
      {
        field: 'deliveryGLN',
        type: 'string',
        displayName: $filter('translate')('DELIVERYGLN'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'lineNumber',
        type: 'string',
        displayName: $filter('translate')('LINENO'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false,
        maxWidth: 75,
      },
      {
        field: 'supplierTUEAN',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERTUEAN'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {field: 'supplierItemCode',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERITEMCODE'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'buyerItemCode',
        type: 'string',
        displayName: $filter('translate')('BUYERITEMCODE'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'quantity',
        type: 'number',
        displayName: $filter('translate')('QUANTITY'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false,
        maxWidth: 90,
      },
      {
        field: 'quantityToAdd',
        type: 'number',
        displayName: $filter('translate')('ADD'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: true,
        maxWidth: 90,
      },
      {
        field: 'description',
        type: 'string',
        displayName: $filter('translate')('DESCRIPTION'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      }
    ],

    shippingNoteEditAsnItems: [
      {
        field: 'packNumber',
        type: 'string',
        displayName: $filter('translate')('PACK'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false,
        cellTemplate: '<div ng-show="row.treeLevel == 0">{{ COL_FIELD CUSTOM_FILTERS }}</div>',
        maxWidth: 75,
        grouping: { groupPriority: 0 },
        sort: { priority: 0,
direction: 'asc' },
      },
      {
        field: 'sscc',
        type: 'string',
        displayName: $filter('translate')('SSCC'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: true,
        /*cell template flags sscc as 'dirty' when changed by the user to allow the original sscc to be used again*/
        cellTemplate: '<input ng-change="grid.appScope.makeDirty(\'{{ grid.appScope.ssccList\[row.treeNode.children[0].row.entity.packNumber - 1\].sscc }}\',\
                                                                      grid.appScope.ssccList\[row.treeNode.children[0].row.entity.packNumber - 1\])"\
                              ng-model="grid.appScope.ssccList\[row.treeNode.children[0].row.entity.packNumber - 1\].sscc" ng-show="row.treeLevel == 0" />',
        width: 155,
      },
      {
        field: 'packType',
        type: 'string',
        displayName: $filter('translate')('PACKTYPE'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false,
        cellTemplate: '<div ng-show="row.treeLevel == 0">{{ grid.appScope.packTypeList\[row.treeNode.children[0].row.entity.packNumber - 1\] }}</div>',
        maxWidth: 100,
      },
      {
        field: 'quantity',
        type: 'number',
        displayName: $filter('translate')('QUANTITY'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: true,
        maxWidth: 90,
      },
      {
        field: 'supplierItemCode',
        type: 'string',
        displayName: $filter('translate')('SUPPLIERITEMCODE'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      },
      {
        field: 'description',
        type: 'string',
        displayName: $filter('translate')('DESCRIPTION'),
        headerCellClass: 'ui-grid-header',
        cellEditableCondition: false
      }
    ]
	}

	var getColumnDefs = function(columnDefsName){
		return columnDefs[columnDefsName];
	}

	return {
		getColumnDefs: getColumnDefs
	}
})