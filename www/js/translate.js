app.config(['$translateProvider', function ($translateProvider) {

  $translateProvider
    .translations('en', {
      //UI Translations
      'ALLSENSORS'          : 'All Sensors',
      'FUNCTIONS'           : 'Functions',
      'DATETIME'            : 'Date Time',
      'PARTICLESENSOR'      : 'Laser Light Particle Sensors',
      'SENSORID'            : 'Sensor ID',
      'SENSORTYPE'          : 'Sensor Type',
      'TEMP'                : 'Temperature',
      'HUMID'               : 'Humidity',
      'PM2.5'               : 'PM 2.5',
      'ACKORDER'            : 'Acknowledge Order',
      'ACTIONS'             : 'Actions',
      'ADDITEM'             : 'Add Item',
      'ADDRESSLINE'         : 'Address Line',
      'ARCHIVE'             : 'Archive',
      'ARCHIVED'            : 'Archived',
      'ATTRIBUTE'           : 'Attribute',
      'BACK'                : 'Back',
      'BRITISHPOUND'        : 'British Pound',
      'CANCEL'              : 'Cancel',
      'CCTITLE'             : 'CC Air Interface',
      'CITY'                : 'City/Country/Province:',
      'CREATECREDITNOTE'    : 'Create Credit Note',
      'CREATENEW'           : 'Create New',
      'CREATEPO'            : 'Create Purchase Order',
      'CREATED'             : 'Created',
      'CREATEDLVRY'         : 'Create Delivery',
      'CREATEINVOICE'       : 'Create Invoice',
      'CREATEASN'           : 'Create ASN',
      'CREATEMSG'           : 'Create Message',
      'CUSTOMERLIST'        : 'Customer\'s List',
      'DATE'                : 'Date',
      'DETAILS'             : 'Details',
      'DVRYSCHEDULE'        : 'Delivery Schedule',
      'EDIT'                : 'Edit',
      'CANCELINVOICE'       : 'Cancel Invoice',
      'EDITINVOICE'         : 'Edit Invoice',
      'EDITPO'              : 'Edit Purchase Order',
      'EDITORDER'           : 'Edit Order',
      'EXPORT'              : 'Export',
      'ITEMSELECT'          : 'item selected',
      'ITEMSSELECT'         : 'items selected',
      'LANG_EN'             : 'English',
      'LANG_ZH-CN'          : '简体',
      'LANG_ZH-HK'          : '繁體',
      'LASTUPD'             : 'Last Update',
      'MSGACTIONS'          : 'Message Actions',
      'MSGINBOX'            : 'Message Inbox',
      'MSGTYPE'             : 'Message Type',
      'NEWORDER'            : 'New Order',
      'NEXT'                : 'Next',
      'NEWINVOICE'          : 'New Invoice',
      'ORDERNO'             : 'Order Number',
      'ORDERSTATUS'         : 'Order Status',
      'ORDERS'              : 'Orders',
      'OTHERINFO'           : 'Other information',
      'PAYMENTTERMS'        : 'Payment Terms',
      'PAYBYDATE'           : 'Pay by specified date',
      'POSTCODE'            : 'Post Code:',
      'PRINT'               : 'Print',
      'REFERENCE'           : 'Reference',
      'REMOVE'              : 'Remove',
      'REPLY'               : 'Reply',
      'SAVE'                : 'Save',
      'SAVEDRAFTINVOICE'    : 'Save Draft Invoice and Close',
      'SHOWPRODUCTS'        : 'Show Products',
      'STATUS'              : 'Status',
      'SUBMITASN'           : 'Submit ASN Now',
      'SUBMITINVOICE'       : 'Submit Invoice Now',
      'SUBMITTED'           : 'submitted',
      'TRDBUYERS'           : 'Trading Partners',
      'VIEW'                : 'View',
      'VILLAGE'             : 'Village/Town/Borough:',
      'WAREHOUSECODE'       : 'Warehouse Code',
      'WAREHOUSELOCATION'   : 'Warehouse Location',
      'USERTYPE'            : 'User Type',
      'CCUSERTITLEC'        : 'CC WebForms Create User (Administrator Mode)',
      'CCUSERTITLEV'        : 'CC WebForms User (Administrator Mode)',
      'CCUSERTITLEE'        : 'CC WebForms Edit User (Administrator Mode)',
      'CCUSERTITLECS'        : 'CC WebForms Create User (Super User Mode)',
      'CCUSERTITLEVS'        : 'CC WebForms User (Super User Mode)',
      'CCUSERTITLEES'        : 'CC WebForms Edit User (Super User Mode)',
      'REVISION'            : 'Revision',
      'REVISEDBY'           : 'Revised by',
      'DATETIME'            : 'Date / Time',
      'STATUS'              : 'Status',
      'DESCRIPTIONOFCHANGE' : 'Description of Change',
      'SELECT'              : 'Select',
      'DELETE'              : 'Delete',
      'SUBMIT'              : 'Submit',
      'REJECT'              : 'Reject',

      //Footer translations
      'ASNSTATUS'           : 'ASN Status',
      'CREATIONDATE'        : 'Creation Date',
      'EDIREF'              : 'EDI references',
      'FILECREATIONDATE'    : 'File Creation Date',
      'FILEVERSIONNO'       : 'File Version Number',
      'FILEGENNO'           : 'File Generation Number',
      'FORMSTATUS'          : 'Form Status',
      'MSGREF'              : 'Message Reference',
      'TEST'                : 'Test',
      'TRANSACTIONREF'      : 'Transaction Reference',
      'ACTIVE'              : 'Active',
      'INACTIVE'            : 'Inactive',
      'UNKNOWN'             : 'Unknown',
      'IMPORTDATE'          : 'Import Date',

      //wMainMenu translation
      'ALLDOCS'             : 'All Documents',
      'ARCHIVES'            : 'Archives',
      'DOCSARCHIVE'         : 'Document Archive',
      'DVRYNOTEASN'         : 'Delivery Note(ASN)',
      'DRAFTS'              : 'Drafts',
      'DRAFTMSGS'           : 'Draft Messages',
      'EDITPROFILE'         : 'Edit Profile',
      'INBOX'               : 'Inbox',
      'OTHERACTIONS'        : 'Other Actions',
      'PROFILESETTINGS'     : 'Profile Settings',
      'SALESSTOCKREPORT'    : 'Sales/Stock Report',
      'SENT'                : 'Sent',
      'SIGNOUT'             : 'Sign Out',
      'ADMIN'               : 'Administrator Functions',
      'PRODUCT'             : 'Products',
      'CREATEPRODUCT'       : 'Create Products',
      'USER'                : 'Users',
      'CREATEUSER'          : 'Create Users',
      'SYSTEM'              : 'System',


      //Login Translations
      'WEBFORMS'            : 'CC WebForms Management Solution',
      'EMAIL'               : 'Email',
      'PASSWORD'            : 'Password',
      'LOGIN'               : 'Login',
      'VALLOGINEMAILREQUIRED': 'You cannot Login without Email Address.',
      'VALLOGINEMAILPATTERN': 'You cannot Login with wrong Email Address pattern.',
      'VALLOGINPASSWORDREQUIRED': 'You cannot Login without Password.',
      'VALLOGINPASSWORDPATTERN': 'Password should be at least 8 digits and contain at least one number, one lowercase and one uppercase character.',

      //Line items selector translations
      'SELECTLINEITEMS'     : 'Select Line Items',

      //Invoice translation
      'DAYSTOPAYMENT'       : 'Days to Payment',
      'SETTLEMENTDISCOUNT'  : 'Settlement Discount %',
      'PAYMENTDATE'         : 'Payment Date',
      'PAYMENTTERMS'        : 'Payment Terms',
      'TAXDATE'             : 'Tax Date',
      'INVOICE'             : 'Invoice',
      'INVOICENO'           : 'Invoice Number',
      'INVOICES'            : 'Invoices',
      'INVOICEDATE'         : 'Invoice Date',
      'INVOICEDETAILS'      : 'Invoice Details',
      'INVOICENUMBER'       : 'Invoice Number',
      'INVSTATUS'           : 'Invoice Status',
      'INVOICESUBMITTED'    : 'Invoice submitted',
      'INVOICESAVED'        : 'Invoice saved',
      'VATBREAKDOWN'        : 'VAT Breakdown',
      'TOTALS'              : 'Totals',
      'CODE'                : 'Code',
      'PERCENT'             : 'Percent',
      'LINES'               : 'Lines',
      'TOTALNET'            : 'Total Net',
      'VATAMOUNT'           : 'VAT Amount',
      'TOTAL'               : 'Total',
      'LINESNET'            : 'Lines net',
      'LINETOTALAMOUNT'     : 'Line Total Amount (ex. VAT)',
      'TOTALTAXABLEAMOUNT'  : 'Total Taxable Amount',
      'TOTALVATAMOUNT'      : 'Total VAT Amount',
      'TOTALINVOICEAMOUNT'  : 'Total Invoice Amount (incl. VAT)',
      'GOODSRECEIPTDATE'    : 'Goods Receipt Date',
      'GOODSRECEIPTNO'      : 'Goods Receipt Number',
      'DVRYNOTEDATE'        : 'Delivery Note Date',
      'DVRYNOTENO'          : 'Delivery Note Number',
      'CARRIERSCAC'         : 'Carrier SCAC',
      'SURCHARGES'          : 'Surcharges',
      'SURCHARGESTYPE'      : 'Surcharges type',
      'SURCHARGESVATCODE'   : 'Surcharges VAT Code',
      'TOTALSURCHARGES'     : 'Total surcharges',
      'DELETEINVOICE'       : 'Delete Invoice',

      //Delivery translations
      'EARLIESTDVRY'        : 'Earliest Delivery',
      'EARLIESTDATE'        : 'Earliest Date',
      'EARLIESTTIME'        : 'Earliest Time',
      'LATESTDVRY'          : 'Latest Delivery',
      'LATESTDATE'          : 'Latest Date',
      'LATESTTIME'          : 'Latest Time',
      'CITY'                : 'City/Country/Province',
      'STREETNAME'          : 'Street Name',
      'HOUSENO'             : 'House/Bldg No',
      'POSTCODE'            : 'Post code',
      'VILLAGE'             : 'Village/Town/Borogh',
      'DVRYADDR'            : 'Delivery Address',
      'DVRYCODE'            : 'Delivery Code',
      'DVRYGLN'             : 'Delivery GLN/DUNS Number',
      'DVRYLOCATION'        : 'Delivery Location',
      'DVRYLOCATIONS'       : 'Delivery Locations',
      'DVRYLOCATIONCODE'    : 'Delivery Location Code',
      'DVRYLOCATIONGLN'     : 'Delivery Location GLN',
      'LOCATION'            : 'Location',
      'DELIVERYGLN'         : 'Delivery GLN',


      //Buyer translations
      'BUYER'               : 'Buyer',
      'BUYERS'              : 'Buyers',
      'BUYERGLN'            : 'Buyer GLN',
      'BUYERHQADDR'         : 'Buyer HQ Address',
      'BUYERITEMCODE'       : 'Buyer Item Code',
      'BUYERNAME'           : 'Buyer Name',
      'BUYERVAT'            : 'Buyer VAT:',
      'BUYERGLNDUNS'        : 'Buyer GLN/DUNS Number',
      'BUYERBILLINGADDR'    : 'Customer (Billing Address)',
      'BILLINGADDRESS'      : 'Billing Address',
      'BILLINGPOSTCODE'     : 'Billing Postcode',


      //Supplier translations
      'SUPPLIER'            : 'Supplier',
      'SUPPLIERNAME'        : 'Supplier Name',
      'SUPPLIERCODE'        : 'Supplier Code',
      'SUPPLIERNUMBER'      : 'Supplier Number',
      'SUPPLIERGLN'         : 'Supplier GLN/DUNS Number',
      'SUPPLIERHQADDR'      : 'Supplier HQ Address',
      'SUPPLIERADDRESS'     : 'Supplier Address',
      'SUPPLIERPOSTCODE'    : 'Supplier Postcode',
      'SUPPLIERITEMCODE'    : 'Supplier Item Code',
      'SUPPLIERLOCATIONCODE': 'Supplier Location Code',
      'SUPPLIERREFCODE'     : 'Supplier Ref Code',
      'SUPPLIERTUEAN'       : 'Supplier TU EAN',
      'SUPPLIERVAT'         : 'Supplier VAT',

      //Instruction Translations
      'CUSTOMERINSTRUCTIONS': 'Customer Instructions',
      'INSTRUCTIONNO'       : 'Instruction Number:',
      'CUSTOMERREMARKS'     : 'Customer remarks:',

      //Order translations
      'ORDERDATE'           : 'Order Date',
      'ORDERNO'             : 'Order Number',
      'ORDER'               : 'Order',
      'PODETAILS'           : 'Purchase Order Details',
      'DRAFT'               : 'Draft',
      'INPROGRESS'          : 'In Progress',
      'VIEWED'              : 'Viewed',
      'NOINVOICE'           : 'No Invoice',
      'ORDERDRAFTCREATED'   : 'Order Draft Created',
      'MODIFIED'            : 'Modified',
      'ORDERMODIFIED'       : 'Order modified',
      'ORDERFIRSTOPENED'    : 'Order first opened',
      'SAVED'               : 'saved',
      'RELEASED'            : 'Released',
      'ORDERRELEASED'       : 'Order Released',


      //Order types translation
      'ORDERTYPE'           : 'Order Type:',
      'NEWORDER'            : 'New Order',
      'FORECASTORDER'       : 'Forecast Order',
      'CANCELLATION'        : 'Cancellation',
      'DROPSHIP'            : 'Drop Ship',
      'DIRECTSHIP'          : 'Direct Ship',
      'STANDALONESHIP'      : 'Standalone Ship',

      //Currency
      'CURRENCY'            : 'Currency',
      'USDOLLAR'            : 'US Dollar',
      'BRITISHPOUND'        : 'British Pound',
      'EURO'                : 'EURO',

      //itemlist translations
      'PRODUCTLIST'         : 'Product List',
      'ITEMLIST'            : 'Item list',
      'ACTIONBUTTON'        : 'Action Button',
      'QUANTITY'            : 'Quantity',
      'DESCRIPTION'         : 'Description',
      'ITEMPRICE'           : 'Item Price',
      'CUINTU'              : 'CU in TU',
      'LINENO'              : 'Line Number',
      'MEASUREUNIT'         : 'Measure unit',
      'MEASUREINDICATOR'    : 'Measure indicator',
      'PRODUCTIDENTITY'     : 'Product Identity',
      'SUPPLIERCUEAN'       : 'Supplier CU EAN',
      'PRODUCTDETAILS'      : 'Product Details',
      'EXTLINECOST'         : 'Ext. Line Cost',
      'VATCODE'             : 'VAT Code',
      'ITEMREMARKS'         : 'Item Remarks',
      'ADDITIONALID'        : 'Additional identification',


      //ASN translations
      'DVRYINFO'            : 'Delivery Information',
      'DVRYNOTE'            : 'Delivery Note',
      'DVRYNOTES'           : 'Delivery Notes',
      'DVRYTIME'            : 'Delivery Time',
      'DOCUMENTDATE'        : 'Document Date',
      'DOCUMENTINFO'        : 'Document Information',
      'DOCUMENTNO'          : 'Document Number',
      'DIRECT'              : 'Direct',
      'CROSSDOCK'           : 'Cross-dock',
      'DVRYDATE'            : 'Delivery Date',
      'SHIPMENTTYPE'        : 'Shipment Type',
      'SHIPMENTREF'         : 'Shipment Reference',
      'ADDSELECTEDTO'       : 'Add Selected To',
      'PALLET'              : 'Pallet',
      'NEW'                 : 'New',
      'CARTON'              : 'Carton',
      'SHIPPINGUNITS'       : 'Shipping Units / Packages',
      'DELETESELECTED'      : 'Delete Selected Items',
      'CARRIERNAME'         : 'Carrier Name',
      'CARRIERGLN'          : 'Carrier GLN',
      //Country translation
      'COUNTRY'             : 'Country',
      //TODO Fix the typo below. //DONE
      //TODO Country translations.
      'ADD' : 'Add',
      'PACK' : 'Pack',
      'ASN' : 'ASN',
      'PACKTYPE' : 'Pack Type',
      'SSCC' : 'SSCC',
      'SHIPPINGNOTERELEASED' : 'Shipping Note Released',
      'SHIPPINGNOTESAVED' : 'Shipping Note Saved',
      'ASNNUMBER' : 'ASN number',


      //Validation Messages
      'VALREQUIRED'         : 'This field is required.',

      //Pop-up Messages
      'OK'  : 'OK',
      'CONFIRM' : 'Confirm',
      'CONTINUE' : 'Continue',
      'CANCEL' : 'Cancel',
      'YES' : 'Yes',
      'NO' : 'No',
      'DELETED' : 'Deleted',
      'REJECTED' : 'Rejected',
      'CANCELLED' : 'Canceled',
      'ERROR' : 'Error',
      'WARNING' : 'Warning',
      'IN' : 'in',
      'LINE' : 'Line',

      'TIMEDOUTTITLE' : 'Timeout',
      'TIMEDOUTTEXT' : 'Session timed out, going back to login screen.',
      'UNAUTHORIZEDTITLE' : 'Unauthorized',
      'UNAUTHORIZEDTEXT' : 'You are not authorized to perform this action, going back to login screen.',
      'UNKNOWNERRORTITLE' : 'Unknown Error',
      'STATUSCODE' : 'Status code',

      'MISSINGFIELDSTITLE' : 'Form Incomplete',
      'MISSINGFIELDSTEXT'  : 'Please enter values in the following required fields',

      'ORDERSUBMITTEDTITLE' : 'Order Submitted.',
      'DELORDERTITLE': 'Delete Order?',
      'DELORDERACTIVETEXT' : 'This will delete the active order.',
      'DELORDERSELECTEDTEXT' : 'This will delete the selected order(s).',
      'REJECTORDERTITLE': 'Reject Order?',
      'REJECTORDERACTIVETEXT' : 'This will mark the active order as rejected.',
      'CANCELORDERTITLE': 'Cancel Order?',
      'CANCELORDERACTIVETEXT' : 'This will mark the active order as canceled.',
      'ORDERSAVEDTITLE' : 'Order Saved',
      'ORDERNUMBERTEXT' : 'Order number',
      // 'ORDERNUMBERSTEXT' : 'Order numbers',

      'ORDACKSUBMITTEDTITLE' : 'Order Acknowledgment Submitted.',
      'DELORDACKTITLE': 'Delete Order Acknowledgment?',
      'DELORDACKACTIVETEXT' : 'This will delete the active acknowledgment.',
      'DELORDACKSELECTEDTEXT' : 'This will delete the selected acknowledgment(s).',
      // 'REJECTORDACKTITLE': 'Reject Order Acknowledgment?', // are below required?
      // 'REJECTORDACKACTIVETEXT' : 'This will mark the active acknowledgment as rejected.',
      // 'CANCELORDACKTITLE': 'Cancel Order Acknowledgment?',
      // 'CANCELORDACKACTIVETEXT' : 'This will mark the active acknowledgment as canceled.',
      'EDITORDACKTITLE' : 'Make Amendments?',
      'EDITORDACKTEXT' : 'Would you like to edit the order acknowledgment?',

      'ASNSUBMITTEDTITLE' : 'ASN Submitted.',
      'ASNSAVEDRAFTTITLE' : 'ASN Saved as Draft',
      'DELASNTITLE': 'Delete ASN?',
      'DELASNACTIVETEXT' : 'This will delete the active ASN.',
      'DELASNSELECTEDTEXT' : 'This will delete the selected ASN(s).',
      'REJECTASNTITLE': 'Reject ASN?',
      'REJECTASNACTIVETEXT' : 'This will mark the active ASN as rejected.',
      'CANCELASNTITLE': 'Cancel ASN?',
      'CANCELASNACTIVETEXT' : 'This will mark the active ASN as canceled.',

      // 'CREATEINVOICETITLE' : 'Create Invoice?' // do we need dialog for these 3?
      // 'CREATEINVOICEACTIVETEXT' : 'This will create an invoice for the active order.',
      // 'CREATEINVOICESELECTEDTEXT' : 'This will create an invoice for the selected order.',
      'INVOICESUBMITTEDTITLE' : 'Invoice Submitted',
      'INVOICESAVEDRAFTTITLE' : 'Invoice Saved as Draft',
      'DELINVOICETITLE': 'Delete Invoice?',
      'DELINVOICEACTIVETEXT' : 'This will delete the active invoice.',
      'DELINVOICESELECTEDTEXT' : 'This will delete the selected invoice(s).',
      'INVOICEDELETEDTITLE' : 'Invoice Deleted',
      'DELETIONCANCELLEDTITLE' : 'Deletion Canceled',
      'REJECTINVOICETITLE': 'Reject Invoice?',
      'REJECTINVOICEACTIVETEXT' : 'This will mark the active invoice as rejected.',
      'INVOICEREJECTEDTITLE' : 'Invoice Rejected',
      'REJECTIONCANCELLEDTITLE' : 'Rejection Canceled',
      'CANCELINVOICETITLE': 'Cancel Invoice?',
      'CANCELINVOICEACTIVETEXT' : 'This will mark the active invoice as canceled.',
      'INVOICECANCELLEDTITLE' : 'Invoice Canceled',
      'CANCELLATIONCANCELLEDTITLE' : 'Cancellation Canceled',

      'CREDSUBMITTEDTITLE' : 'Credit Note Submitted.',
      'DELCREDITTITLE': 'Delete Credit Note?',
      'DELCREDITACTIVETEXT' : 'This will delete the active credit note.',
      'DELCREDITSELECTEDTEXT' : 'This will delete the selected credit note(s).',
      'REJECTCREDITTITLE': 'Reject Credit Note?',
      'REJECTCREDITACTIVETEXT' : 'This will mark the active credit note as rejected.',
      'CANCELCREDITTITLE': 'Cancel Credit Note?',
      'CANCELCREDITACTIVETEXT' : 'This will mark the active credit note as canceled.',

      'PRODUCTSAVEDTITLE' : 'Product Saved',
      'PRODUCTSAVEDTEXT' : 'Supplier item code',
      'DELPRODUCTTITLE': 'Delete Product?',
      'DELPRODUCTSELECTEDTEXT' : 'This will delete the selected product(s).',
      'PRODUCTDELETEDTITLE' : 'Products Deleted',
      'PRODUCTDELETEDTEXT' : 'Supplier Item Codes',
      'DELPRODUCTTITLE': 'Delete Variation?',
      'DELPRODUCTACTIVETEXT' : 'This will delete the active variation of this product.',

      'SELECTORDERTITLE' : 'No orders selected',
      'SELECTORDERTEXT' : 'Please select an order.',
      'SELECTINVOICETITLE' : 'No invoice selected',
      'SELECTINVOICETEXT' : 'Please select an invoice.',

      'REMOVELINEITEMTITLE' : 'Remove line item?',
      'REMOVELINEITEMTEXT' : 'This will remove the selected line item(s) from the list.',

      'QTYEXCEEDSTEXT' : 'Chosen quantity exceeds the original on the following lines',

      'ADDDELIVERYLOCATIONTITLE' : 'Add delivery location',
      'ADDDELIVERYLOCATIONTEXT' : 'This will add a new delivery location to your company\'s profile.',
      'DELIVERYLOCATIONUPDATEDTITLE' : 'Delivery Location Updated',
      'DELIVERYLOCATIONUPDATEDTEXT' : 'Selected delivery location has been updated.',
      'INVALIDDELIVERYLOCATIONTITLE' : 'Invalid Delivery Location',
      'INVALIDDELIVERYLOCATIONTEXT' : 'Delivery location name cannot be empty',

      'USERSAVEDTITLE' : 'User Saved',
      'USERSAVEDTEXT' : 'Email address',
      'USEREXISTSTITLE' : 'User already exists!',
      'USEREXISTSTEXT' : 'Please use another email address.',

      'SYSTEMPROFILESAVEDTITLE' : 'System Profile Saved',
      'SYSTEMPROFILESAVEDTEXT' : 'Company name',
      'FILESIZELIMITTITLE' : 'File size limit',
      'FILESIZELIMITTEXT' : 'Logo cannot be larger than 1MB in size.',

    })
    .translations('zh-hk', {
      'ACKORDER':'確認訂單',
      'ACTIONS':'動作',
      'ADDITEM':'新增項目',
      'ARCHIVE':'收藏',
      'ATTRIBUTE':'屬性',
      'BACK':'返回',
      'BRITISHPOUND':'英磅',
      'BUYERGLN':'買家GLN:',
      'BUYERHQADDR':'買家總部地址',
      'BUYERITEMCODE':'買家產品編號',
      'BUYERNAME':'買家名稱',
      'BUYERVAT':'買家稅號',
      'CANCEL':'取消',
      'CCTITLE':'CC 網路表格 - 管理方案',
      'CITY':'城巿/國家/省',
      'CREATECREDITNOTE':'建立信用票據',
      'CREATENEW':'新增',
      'CREATEPO':'建立訂單',
      'CREATED':'已建立',
      'CREATEDLVRY':'建立送貨',
      'CREATEINVOICE':'建立發票',
      'CREATEMSG':'建立訊息',
      'CUSTOMERLIST':'客戶列表',
      'DATE':'日期',
      'EDIT':'修改',
      'EXPORTINVOICE':'匯出發票',
      'EDITINVOICE':'修改發票',
      'EDITPO':'修改訂單',
      'EDITORDER'           : '修改訂單',
      'EXPORT':'匯出',
      'ITEMSELECT':'項目已選擇',
      'ITEMSSELECT':'項目已選擇',
      'LANG_EN':'English',
      'LANG_ZH-CN':'简体',
      'LANG_ZH-HK':'繁體',
      'LASTUPD':'最後更新',
      'MSGACTIONS':'訊息動作',
      'MSGTYPE':'訊息種類',
      'NEWORDER':'新訂單',
      'ORDERNO':'訂單編號:',
      'ORDERSTATUS':'訂單狀態',
      'ORDERS':'訂單',
      'OTHERINFO':'其他資料',
      'PAYMENTDATE':'付款日期',
      'PAYMENTTERMS':'付款條款',
      'POSTCODE':'郵政號碼:',
      'PRINT':'列印',
      'REFERENCE':'參考',
      'REMOVE':'移除',
      'REPLY':'回覆',
      'SAVE':'儲存',
      'SAVEDRAFTINVOICE':'儲存發票草稿',
      'SHOWPRODUCTS':'顯示貨品',
      'STATUS':'狀態',
      'SUBMITINVOICE':'送出發票',
      'TRDBUYERS':'貿易夥伴',
      'VIEW':'檢視',
      'VILLAGE':'村/鎮:',
      'USERTYPE'            : '使用者類型',
      'CCUSERTITLEC'        : 'CC WebForms 建立使用者 (管理人員模式)',
      'CCUSERTITLEV'        : 'CC WebForms 使用者 (管理人員模式)',
      'CCUSERTITLEE'        : 'CC WebForms 編輯使用者 (管理人員模式)',
      'CCUSERTITLECS'        : 'CC WebForms 建立使用者 (進階使用者模式)',
      'CCUSERTITLEVS'        : 'CC WebForms 使用者 (進階使用者模式)',
      'CCUSERTITLEES'        : 'CC WebForms 編輯使用者 (進階使用者模式)',

      //Footer translations
      'CREATIONDATE'        : '建立日期:',
      'EDIREF'              : 'EDI參考:',
      'FILECREATIONDATE'    : '檔案建立日期:',
      'FILEVERSIONNO'       : '檔案版本編號:',
      'FILEGENNO'           : '檔案產生編號:',
      'FORMSTATUS'          : '表格狀態:',
      'MSGREF'              : '訊息參考:',
      'TEST'                : '測試:',
      'TRANSACTIONREF'      : '交易參考:',
      'ACTIVE'              : '活躍',
      'INACTIVE'            : '非活躍',

      //wMainMenu translation
      'ALLDOCS'             : '所有文件',
      'ARCHIVES'            : '封存',
      'DOCSARCHIVE'         : '封存文件',
      'DVRYNOTEASN'         : '預先發貨通知',
      'DRAFTS'              : '草稿',
      'DRAFTMSGS'           : '草稿文件',
      'EDITPROFILE'         : '編輯設定檔案',
      'INBOX'               : '收件箱',
      'OTHERACTIONS'        : '其他動作',
      'PROFILESETTINGS'     : '設定檔案',
      'SALESSTOCKREPORT'    : '銷售/庫存報告',
      'SENT'                : '已發送',
      'SIGNOUT'             : '登出',
      'ADMIN'               : '管理員功能',
      'PRODUCT'             : '貨品',
      'CREATEPRODUCT'       : '建立貨品',
      'USER'                : '使用者',
      'CREATEUSER'          : '建立使用者',
      'SYSTEM'              : '系統',

      //Login Translations:
      'WEBFORMS':'CC 網路表格 管理方案',
      'EMAIL':'電郵',
      'PASSWORD':'密碼',
      'LOGIN':'登入',
      'VALLOGINEMAILREQUIRED': '電郵地址是必需的',
      'VALLOGINEMAILPATTERN': '電郵的格式錯誤',
      'VALLOGINPASSWORDREQUIRED': '密碼是必需的',
      'VALLOGINPASSWORDPATTERN': '密碼必需最少8個字元和最少有1個數字，1個小階和1個大階字元',

      //Invoice translation:
      'DAYSTOPAYMENT':'付款日',
      'SETTLEMENTDISCOUNT':'結算折扣%',
      'TAXDATE':'繳稅日期',
      'INVOICE':'發票',
      'INVOICENO':'發票編號',
      'INVOICES':'發票',
      'INVOICEDATE':'發票日期',
      'INVOICEDETAILS':'發票細節',
      'INVOICENUMBER':'發票編號',
      'INVSTATUS' : '發票狀態',

      //Delivery translations:
      'EARLIESTDVRY':'最早送貨日期',
      'EARLIESTDATE':'最早送貨日期',
      'EARLIESTTIME':'最早送貨時間',
      'LATESTDVRY':'最遲送貨日期',
      'LATESTDATE':'最遲送貨日期',
      'LATESTTIME':'最遲送貨時間',
      'STREETNAME':'街名',
      'HOUSENO':'屋/大廈號碼',
      'POSTCODE':'郵政號碼',
      'VILLAGE':'村/鎮',
      'DVRYADDR':'送貨地址',
      'DVRYCODE':'送貨編號',
      'DVRYGLN':'送貨GLN/DUNS 號碼',
      'DVRYLOCATION':'送貨地點',
      'DVRYLOCATIONS':'送貨地點',
      'DVRYLOCATIONCODE':'送貨地點編號',
      'DVRYLOCATIONGLN':'送貨地點GLN',
      'DVRYNOTEDATE':'送貨單日期',
      'DVRYNOTENO':'送貨單編號',
      'LOCATION':'地點',

      //Buyer translations:
      'BUYER':'買家',
      'BUYERGLN':'買家GLN',
      'BUYERHQADDR':'買家總部地址',
      'BUYERITEMCODE':'買家產品編號',
      'BUYERNAME':'買家名稱',
      'BUYERVAT':'買家稅號',
      'BUYERGLNDUNS':'買家GLN/DUNS',
      'BUYERBILLINGADDR':'買家 (帳單地址)',

      //Supplier translations:
      'SUPPLIER':'供應商',
      'SUPPLIERCODE':'供應商編號',
      'SUPPLIERGLN':'供應商GLN/DUNS編號',
      'SUPPLIERHQADDR':'供應商總部地址',
      'SUPPLIERITEMCODE':'供應商項目編號',
      'SUPPLIERNAME':'供應商名稱',
      'SUPPLIERLOCATIONCODE':'供應商地點編號',
      'SUPPLIERREFCODE':'供應商參考編號:',
      'SUPPLIERTUEAN':'供應商交易單位條碼',
      'SUPPLIERVAT':'供應商稅務編號',

      //Instruction Translations:
      'CUSTOMERINSTRUCTIONS':'客戶說明',
      'INSTRUCTIONNO':'說明號碼:',
      'CUSTOMERREMARKS':'客戶備注:',

      //Order translations:
      'ORDERDATE':'訂單日期',
      'ORDERNO':'訂單編號:',
      'ORDER':'訂單',
      'GOODSRECEIPTDATE':'貨物接收日期',
      'GOODSRECEIPTNO':'貨物接收編號',
      'PODETAILS':'訂單細節',
      'DRAFT':'草稿',
      'INPROGRESS':'處理中',
      'NOINVOICE':'沒有發票',
      'ORDERDRAFTCREATED':'建立訂單草稿',
      'MODIFIED':'被修改',
      'ORDERMODIFIED':'訂單被修改',

      //Order types translation:
      'ORDERTYPE':'訂單種類:',
      'NEWORDER':'新訂單',
      'FORECASTORDER':'預報訂單',
      'CANCELLATION':'取消',
      'DROPSHIP':'下降運輸',
      'DIRECTSHIP':'直接發貨',
      'STANDALONESHIP':'獨立運輸',

      //Currency:
      'CURRENCY':'貨幣',
      'USDOLLAR':'美元',
      'BRITISHPOUND':'英磅',
      'EURO':'歐元',

      //itemlist translations:
      'PRODUCTLIST':'貨品列表',
      'ITEMLIST':'項目列表',
      'ACTIONBUTTON':'動作按鈕',
      'QUANTITY':'數量',
      'DESCRIPTION':'描述',
      'ITEMPRICE':'項目價錢',
      'CUINTU':'每交易單位內的貨量',
      'LINENO':'行號',
      'PRODUCTIDENTITY'     : '貨品編號',
      'SUPPLIERCUEAN'       : '供應商貨品EAN編號',
      'PRODUCTDETAILS'      : '貨品詳細',
      'EXTLINECOST'         : 'Ext. Line Cost',
      'VATCODE'             : '增值稅編號',
      'ITEMREMARKS'         : '貨品備註',

      //Country translation:
      'COUNTRY':'國家',

      //Validation Messages
      'VALREQUIRED'         : '這是必要欄位.',

      'ADDRESSLINE':'地址',
      'ARCHIVED':'封存',
      'DETAILS':'詳細內容',
      'DVRYSCHEDULE':'運送時間表',
      'MSGINBOX':'收件夾',
      'NEXT':'下一步',
      'NEWINVOICE':'新發票',
      'PAYBYDATE':'特定日期付款',
      'SUBMITASN':'發送出貨通知',
      'SUBMITTED':'已發送',
      'WAREHOUSECODE':'貨倉編號',
      'WAREHOUSELOCATION':'貨倉地點',
      'REVISION':'修訂',
      'REVISEDBY':'修改人',
      'DATETIME':'日期 / 時間',
      'DESCRIPTIONOFCHANGE':'變動描述',
      'SELECT':'選擇',
      'ASNSTATUS':'出貨通知狀態',
      'UNKNOWN':'不明',
      'IMPORTDATE':'匯入日期',
      'SELECTLINEITEMS':'選擇項目',
      'INVOICESUBMITTED':'已發送發票',
      'INVOICESAVED':'已儲存發票',
      'VATBREAKDOWN':'增值稅明細',
      'TOTALS':'總計',
      'CODE':'編號',
      'PERCENT':'百份比',
      'LINES':'項目',
      'TOTALNET':'總計淨值',
      'VATAMOUNT':'增值稅額',
      'TOTAL':'總計',
      'LINESNET':'項目淨值',
      'LINETOTALAMOUNT':'項目總額(不包含增值稅)',
      'TOTALTAXABLEAMOUNT':'應納稅總額',
      'TOTALVATAMOUNT':'增值稅總額',
      'TOTALINVOICEAMOUNT':'發票總額(包含增值稅)',
      'CARRIERSCAC':'運送單位SCAC',
      'SURCHARGES':'附加費',
      'SURCHARGESTYPE':'附加費種類',
      'SURCHARGESVATCODE':'附加費增值稅編號',
      'TOTALSURCHARGES':'總附加費',
      'DELIVERYGLN':'運送GLN',
      'BILLINGADDRESS':'帳單地址',
      'BILLINGPOSTCODE':'帳單郵政編號',
      'SUPPLIERNUMBER':'供應商號碼',
      'SUPPLIERADDRESS':'供應商地址',
      'SUPPLIERPOSTCODE':'供應商郵政編號',
      'VIEWED':'已閱讀',
      'ORDERFIRSTOPENED':'訂單第一次被開啟',
      'SAVED':'已儲存',
      'RELEASED':'已放出',
      'ORDERRELEASED':'訂單已放出',
      'ADDITIONALID':'附加識別碼',
      'DVRYINFO':'運送資料',
      'DVRYNOTE':'運送備忘',
      'DVRYNOTES':'運送備忘',
      'DVRYTIME':'運送時間',
      'DOCUMENTDATE':'文件日期',
      'DOCUMENTINFO':'文件資料',
      'DOCUMENTNO':'文件號碼',
      'DIRECT':'直接',
      'CROSSDOCK':'交叉轉運',
      'DVRYDATE':'運送日期',
      'SHIPMENTTYPE':'裝運類別',
      'SHIPMENTREF':'裝運參考',
      'ADDSELECTEDTO':'把選擇的加到',
      'PALLET':'卡板',
      'NEW':'新',
      'CARTON':'紙板箱',
      'SHIPPINGUNITS':'裝運單位/包裝',
      'DELETESELECTED':'刪除已選擇項目',
      'CARRIERNAME':'運送單位名稱',
      'CARRIERGLN':'運送單位GLN',
      'CREATEASN':'建立出貨通知',
      'QUANTITYTOADD':'加數量',
      'PACK':'包裝',
      'ASN':'出貨通知',
      'PACKTYPE':'包裝種類',
      'SSCC':'系列貨運包裝箱代碼SSCC'
    })
    .translations('zh-cn', {
      'ACKORDER':'确认订单',
            'ACTIONS':'动作',
            'ADDITEM':'新增项目',
            'ARCHIVE':'收藏',
            'ATTRIBUTE':'属性',
            'BACK':'返回',
            'BRITISHPOUND':'英磅',
            'BUYERGLN':'买家GLN',
            'BUYERHQADDR':'买家总部地址',
            'BUYERITEMCODE':'买家产品编号',
            'BUYERNAME':'买家名称:',
            'BUYERVAT':'买家税号:',
            'CANCEL':'取消',
            'CCTITLE':'CC 网路表格 - 管理方案',
            'CITY':'城巿/国家/省',
            'CREATECREDITNOTE':'建立信用票据',
            'CREATENEW':'新增',
            'CREATEPO':'建立订单',
            'CREATED':'已建立',
            'CREATEDLVRY':'建立送货',
            'CREATEINVOICE':'建立发票',
            'CREATEMSG':'建立讯息',
            'CUSTOMERLIST':'客户列表',
            'DATE':'日期',
            'EDIT':'修改',
            'EXPORTINVOICE':'汇出发票',
            'EDITINVOICE':'修改发票',
            'EDITPO':'修改订单',
            'EDITORDER'           : '修改订单',
            'EXPORT':'汇出',
            'ITEMSELECT':'项目已选择',
            'ITEMSSELECT':'项目已选择',
            'LANG_EN':'English',
            'LANG_ZH-CN':'简体',
            'LANG_ZH-HK':'繁体',
            'LASTUPD':'最后更新',
            'MSGACTIONS':'讯息动作',
            'MSGTYPE':'讯息种类',
            'NEWORDER':'新订单',
            'ORDERNO':'订单编号:',
            'ORDERSTATUS':'订单状态',
            'ORDERS':'订单',
            'OTHERINFO':'其他资料',
            'PAYMENTDATE':'付款日期',
            'PAYMENTTERMS':'付款条款',
            'POSTCODE':'邮政号码',
            'PRINT':'列印',
            'REFERENCE':'参考',
            'REMOVE':'移除',
            'REPLY':'回覆',
            'SAVE':'储存',
            'SAVEDRAFTINVOICE':'储存发票草稿 && 关闭',  // please replace '&&'
            'SHOWPRODUCTS':'显示货品',
            'STATUS':'状态',
            'SUBMITINVOICE':'送出发票',
            'TRDBUYERS':'贸易伙伴',
            'VIEW':'检视',
            'VILLAGE':'村/镇:',
            'USERTYPE' : '使用者类型',
            'CCUSERTITLEC' : 'CC WebForms 建立使用者 (管理人员模式)',
            'CCUSERTITLEV' : 'CC WebForms 使用者 (管理人员模式)',
            'CCUSERTITLEE' : 'CC WebForms 编辑使用者 (管理人员模式)',
            'CCUSERTITLECS' : 'CC WebForms 建立使用者 (进阶使用者模式)',
            'CCUSERTITLEVS' : 'CC WebForms 使用者 (进阶使用者模式)',
            'CCUSERTITLEES' : 'CC WebForms 编辑使用者 (进阶使用者模式)',

            //Footer translations
            'CREATIONDATE' : '建立日期:',
            'EDIREF' : 'EDI参考:',
            'FILECREATIONDATE' : '档案建立日期:',
            'FILEVERSIONNO' : '档案版本编号:',
            'FILEGENNO' : '档案产生编号:',
            'FORMSTATUS' : '表格状态:',
            'MSGREF' : '讯息参考:',
            'TEST' : '测试:',
            'TRANSACTIONREF' : '交易参考:',
            'ACTIVE' : '活跃',
            'INACTIVE' : '非活跃',

            //wMainMenu translation
            'ALLDOCS':'所有文件',
            'ARCHIVES':'封存',
            'DOCSARCHIVE':'封存文件',
            'DVRYNOTEASN':'预先发货通知',
            'DRAFTS':'草稿',
            'DRAFTMSGS':'草稿文件',
            'EDITPROFILE':'编辑设定档案',
            'INBOX':'收件箱',
            'OTHERACTIONS':'其他动作',
            'PROFILESETTINGS':'设定档案',
            'SALESSTOCKREPORT':'销售/库存报告',
            'SENT':'已发送',
            'SIGNOUT':'登出',
            'ADMIN' : '管理员功能',
            'PRODUCT' : '货品',
            'CREATEPRODUCT' : '建立货品',
            'USER' : '使用者',
            'CREATEUSER' : '建立使用者',
            'SYSTEM' : '系统',

            //Login Translations:
            'WEBFORMS':'CC 网路表格 管理方案',
            'EMAIL':'电邮',
            'PASSWORD':'密码',
            'LOGIN':'登入',
            'VALLOGINEMAILREQUIRED': '电邮地址是必需的',
            'VALLOGINEMAILPATTERN': '电邮的格式错误',
            'VALLOGINPASSWORDREQUIRED': '密码是必需的',
            'VALLOGINPASSWORDPATTERN': '密码必需最少8个字元和最少有1个数字，1个小阶和1个大阶字元',

            //Invoice translation:
            'DAYSTOPAYMENT':'付款日',
            'SETTLEMENTDISCOUNT':'结算折扣%',
            'TAXDATE':'缴税日期',
            'INVOICE':'发票',
            'INVOICENO':'发票编号',
            'INVOICES':'发票',
            'INVOICEDATE':'发票日期',
            'INVOICEDETAILS':'发票细节',
            'INVOICENUMBER':'发票编号',
            'INVSTATUS' : '发票状态',

      //Delivery translations:
            'EARLIESTDVRY':'最早送货日期',
            'EARLIESTDATE':'最早送货日期',
            'EARLIESTTIME':'最早送货时间',
            'LATESTDVRY':'最迟送货日期',
            'LATESTDATE':'最迟送货日期',
            'LATESTTIME':'最迟送货时间',
            'STREETNAME':'街名',
            'HOUSENO':'屋/大厦号码',
            'POSTCODE':'邮政号码',
            'VILLAGE':'村/镇',
            'DVRYADDR':'送货地址',
            'DVRYCODE':'送货编号',
            'DVRYGLN':'送货GLN/DUNS 号码',
            'DVRYLOCATION':'送货地点',
            'DVRYLOCATIONS':'送货地点',
            'DVRYLOCATIONCODE':'送货地点编号',
            'DVRYLOCATIONGLN':'送货地点GLN',
            'DVRYNOTEDATE':'送货单日期',
            'DVRYNOTENO':'送货单编号',
            'LOCATION':'地点:',

            //Buyer translations:
            'BUYER':'买家',
            'BUYERGLN':'买家GLN',
            'BUYERHQADDR':'买家总部地址',
            'BUYERITEMCODE':'买家产品编号',
            'BUYERNAME':'买家名称',
            'BUYERVAT':'买家税号',
            'BUYERGLNDUNS':'买家GLN/DUNS',
            'BUYERBILLINGADDR':'买家 (帐单地址)',

            //Supplier translations:
            'SUPPLIER':'供应商',
            'SUPPLIERCODE':'供应商编号',
            'SUPPLIERGLN':'供应商GLN/DUNS编号',
            'SUPPLIERHQADDR':'供应商总部地址',
            'SUPPLIERITEMCODE':'供应商项目编号',
            'SUPPLIERNAME':'供应商名称',
            'SUPPLIERLOCATIONCODE':'供应商地点编号',
            'SUPPLIERREFCODE':'供应商参考编号',
            'SUPPLIERTUEAN':'供应商交易单位条码',
            'SUPPLIERVAT':'供应商税务编号',

            //Instruction Translations:
            'CUSTOMERINSTRUCTIONS':'客户说明',
            'INSTRUCTIONNO':'说明号码:',
            'CUSTOMERREMARKS':'客户备注:',

            //Order translations:
            'ORDERDATE':'订单日期',
            'ORDERNO':'订单编号',
            'ORDER':'订单',
            'GOODSRECEIPTDATE':'货物接收日期',
            'GOODSRECEIPTNO':'货物接收编号',
            'PODETAILS':'订单细节',
            'DRAFT':'草稿',
            'INPROGRESS':'处理中',
            'NOINVOICE':'没有发票',
            'ORDERDRAFTCREATED':'建立订单草稿',
            'MODIFIED':'被修改',
            'ORDERMODIFIED':'订单被修改',

            //Order types translation:
            'ORDERTYPE':'订单种类:',
            'NEWORDER':'新订单',
            'FORECASTORDER':'预报订单',
            'CANCELLATION':'取消',
            'DROPSHIP':'下降运输',
            'DIRECTSHIP':'直接发货',
            'STANDALONESHIP':'独立运输',

            //Currency:
            'CURRENCY':'货币',
            'USDOLLAR':'美元',
            'BRITISHPOUND':'英磅',
            'EURO':'欧元',

            //itemlist translations:
            'PRODUCTLIST':'货品列表',
            'ITEMLIST':'项目列表',
            'ACTIONBUTTON':'动作按钮',
            'QUANTITY':'数量',
            'DESCRIPTION':'描述',
            'ITEMPRICE':'项目价钱',
            'CUINTU':'每交易单位内的货量',
            'LINENO':'行号',
            'PRODUCTIDENTITY'     : '貨品編號',
            'SUPPLIERCUEAN'       : '供應商貨品EAN編號',
            'PRODUCTDETAILS'      : '貨品詳細',
            'EXTLINECOST'         : 'Ext. Line Cost',
            'VATCODE'             : '增值稅編號',
            'ITEMREMARKS'         : '貨品備註',

            //Country translation:
            'COUNTRY':'国家',

            //Validation Messages
            'VALREQUIRED' : '这是必要栏位.',

            'ADDRESSLINE':'地址',
            'ARCHIVED':'封存',
            'DETAILS':'详细内容',
            'DVRYSCHEDULE':'运送时间表',
            'MSGINBOX':'收件夹',
            'NEXT':'下一步',
            'NEWINVOICE':'新发票',
            'PAYBYDATE':'特定日期付款',
            'SUBMITASN':'发送出货通知',
            'SUBMITTED':'已发送',
            'WAREHOUSECODE':'货仓编号',
            'WAREHOUSELOCATION':'货仓地点',
            'REVISION':'修订',
            'REVISEDBY':'修改人',
            'DATETIME':'日期 / 时间',
            'DESCRIPTIONOFCHANGE':'变动描述',
            'SELECT':'选择',
            'ASNSTATUS':'出货通知状态',
            'UNKNOWN':'不明',
            'IMPORTDATE':'汇入日期',
            'SELECTLINEITEMS':'选择项目',
            'INVOICESUBMITTED':'已发送发票',
            'INVOICESAVED':'已储存发票',
            'VATBREAKDOWN':'增值税明细',
            'TOTALS':'总计',
            'CODE':'编号',
            'PERCENT':'百份比',
            'LINES':'项目',
            'TOTALNET':'总计净值',
            'VATAMOUNT':'增值税额',
            'TOTAL':'总计',
            'LINESNET':'项目净值',
            'LINETOTALAMOUNT':'项目总额(不包含增值税)',
            'TOTALTAXABLEAMOUNT':'应纳税总额',
            'TOTALVATAMOUNT':'增值税总额',
            'TOTALINVOICEAMOUNT':'发票总额(包含增值税)',
            'CARRIERSCAC':'运送单位SCAC',
            'SURCHARGES':'附加费',
            'SURCHARGESTYPE':'附加费种类',
            'SURCHARGESVATCODE':'附加费增值税编号',
            'TOTALSURCHARGES':'总附加费',
            'DELIVERYGLN':'运送GLN',
            'BILLINGADDRESS':'帐单地址',
            'BILLINGPOSTCODE':'帐单邮政编号',
            'SUPPLIERNUMBER':'供应商号码',
            'SUPPLIERADDRESS':'供应商地址',
            'SUPPLIERPOSTCODE':'供应商邮政编号',
            'VIEWED':'已阅读',
            'ORDERFIRSTOPENED':'订单第一次被开启',
            'SAVED':'已储存',
            'RELEASED':'已放出',
            'ORDERRELEASED':'订单已放出',
            'ADDITIONALID':'附加识别码',
            'DVRYINFO':'运送资料',
            'DVRYNOTE':'运送备忘',
            'DVRYNOTES':'运送备忘',
            'DVRYTIME':'运送时间',
            'DOCUMENTDATE':'文件日期',
            'DOCUMENTINFO':'文件资料',
            'DOCUMENTNO':'文件号码',
            'DIRECT':'直接',
            'CROSSDOCK':'交叉转运',
            'DVRYDATE':'运送日期',
            'SHIPMENTTYPE':'装运类别',
            'SHIPMENTREF':'装运参考',
            'ADDSELECTEDTO':'把选择的加到',
            'PALLET':'卡板',
            'NEW':'新',
            'CARTON':'纸板箱',
            'SHIPPINGUNITS':'装运单位/包装',
            'DELETESELECTED':'删除已选择项目',
            'CARRIERNAME':'运送单位名称',
            'CARRIERGLN':'运送单位GLN',
            'CREATEASN':'建立出货通知',
            'QUANTITYTOADD':'加数量',
            'PACK':'包装',
            'ASN':'出货通知',
            'PACKTYPE':'包装种类',
            'SSCC':'系列货运包装箱代码SSCC'
    })
    .preferredLanguage('en')
    .useSanitizeValueStrategy('escapeParameters');

}]);
