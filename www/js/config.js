app.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
  $urlRouterProvider.otherwise('/');
  $ionicConfigProvider.views.maxCache(0);
  $stateProvider
  .state('index',{
    url: '/',
    templateUrl: 'lasersensor.html',
    controller: 'indexCtrl'
  })

  .state('lasersensor',{
	  url:'/lasersensor',
    templateUrl: 'view/wFormMenu.html',
    controller: 'wFormMenuCtrl'
  })

  .state('lasersensor.tab',{
    url:'/tab',
    views: {
      'content':{
        templateUrl: 'view/wTab.html',
        controller: 'wTabCtrl'
      }
    }
  })

  .state('lasersensor.tab.main',{
    url: '/main',
    templateUrl: 'view/wFormMain.html',
	  controller: 'wFormMainCtrl'
  })

})
