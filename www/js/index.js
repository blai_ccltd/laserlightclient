/*****************************************************************
* Module Name: index
* Description: index
* Author: BLai
* Date Modified: 2016.07.26
* Related files: index.html
*****************************************************************/
app.controller('indexCtrl', function($state, $rootScope) {
    //If this is not mobile device, force to login screen.
    console.log(ionic.Platform.device().model);
    if((!ionic.Platform.isIOS() && !ionic.Platform.isAndroid() && !ionic.Platform.isIPad()) || ionic.Platform.device().model === "x86_64"){
      window.localStorage.setItem('mobile', '0');
    }	else{
      window.localStorage.setItem('mobile', '1');
    }
    $rootScope.regLoad();
});
