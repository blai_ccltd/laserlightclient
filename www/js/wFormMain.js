/*****************************************************************
* Module Name: Web Form Main Menu
* Description: To display content of main webform
* Author: dredmonds
* Date Modified: 2016.09.23
* Related files: wFormMain.html
*****************************************************************/

app.controller('wFormMainCtrl', function($scope, $rootScope, $state, $stateParams, $ionicPopup, $window, $ionicHistory, $mdEditDialog, $q, $timeout, $mdSidenav, $mdToast, $filter, lineItemsStagingService, dialogService, httpService) {
  window.$scope = $scope;

  $scope.sensor1 = {};
  $scope.sensor2 = {};

  function timeout() {
      setTimeout(function () {
          // Do Something Here
          // Then recall the parent function to
          // create a recursive loop.
          $scope.loadStuff();
          timeout();
      }, 10000);
  }

  $scope.loadStuff = function () {
    $scope.promise = $timeout(function (){
      $scope.mobile = window.localStorage.getItem('mobile')=='1'?true:false;

      let reqObj1 = {
        sensorID: 'A001'
      }
      httpService.postReq("/sensorLatest", reqObj1).then(function(result){
        if (result.success){
          let createdDate = new Date(result.createdDate);
          $scope.sensor1 = {
            sensorID : result.sensorID,
            sensorType  : result.sensorType,
            reading1: result.reading1,
            reading2: result.reading2,
            reading3: result.reading3,
            createdDate: createdDate.toLocaleString()
          };
          console.log($scope.sensor1);
        }
      });

      let reqObj2 = {
        sensorID: 'A002'
      }
      httpService.postReq("/sensorLatest", reqObj2).then(function(result){
        if (result.success){
          let createdDate = new Date(result.createdDate);
          $scope.sensor2 = {
            sensorID : result.sensorID,
            sensorType  : result.sensorType,
            reading1: result.reading1,
            reading2: result.reading2,
            reading3: result.reading3,
            createdDate: createdDate.toLocaleString()
          };
          console.log($scope.sensor2);
        }
      });

      let reqObj3 = {
        sensorID: '020021D8'
      }
      httpService.postReq("/sensorLatest", reqObj3).then(function(result){
        if (result.success){
          let createdDate = new Date(result.createdDate);
          $scope.sensor3 = {
            sensorID : result.sensorID,
            sensorType  : result.sensorType,
            reading1: result.reading1,
            createdDate: createdDate.toLocaleString()
          };
          console.log($scope.sensor3);
        }
      });

      let reqObj4 = {
        sensorID: '02001F9B'
      }
      httpService.postReq("/sensorLatest", reqObj4).then(function(result){
        if (result.success){
          let createdDate = new Date(result.createdDate);
          $scope.sensor4 = {
            sensorID : result.sensorID,
            sensorType  : result.sensorType,
            reading1: result.reading1,
            createdDate: createdDate.toLocaleString()
          };
          console.log($scope.sensor4);
        }
      });
    }, 2000);
  };

  function stateGoFunction(msgType, msgID, documentNumber){
    switch(msgType.toUpperCase()){
      case "ORDER":
        console.log('StateGo Order Details');
        $rootScope.addNavPage({ statePath:"webForm.tab.orderDetails({msgID:'"+ msgID +"'})",
                                name:'orderDetails' + documentNumber,
                                title:$filter('translate')('ORDER') + " " + documentNumber,
                                state:'webForm.tab.orderDetails',
                                param:{'msgID': msgID}});
        break;
      case "ASN":
        console.log('StateGo Shipping Note Details');
        $rootScope.addNavPage({ statePath:"webForm.tab.shippingNoteDetails({msgID:'"+ msgID +"'})",
                                name:'shippingNoteDetails' + documentNumber,
                                title:$filter('translate')('SHIPPINGNOTE') + " " + documentNumber,
                                state:'webForm.tab.shippingNoteDetails',
                                param:{'msgID': msgID}});
        break;
      case "INVOICE":
        console.log('StateGo Invoice Details');
        $rootScope.addNavPage({ statePath:"webForm.tab.invoiceDetails({msgID:'"+ msgID +"'})",
                                name:'invoiceDetails' + documentNumber,
                                title:$filter('translate')('INVOICE') + " "+ documentNumber,
                                state:'webForm.tab.invoiceDetails',
                                param: {'msgID': msgID, 'name':"invoiceDetails" + documentNumber}});
        break;
      case "ORDERACK":
        console.log('StateGo Order ACK Details');
        $rootScope.addNavPage({ statePath:"webForm.tab.orderAckDetails({msgID:'"+ msgID +"'})",
                                name:'orderAckDetails' + documentNumber,
                                title:$filter('translate')('ORDERACK')+ " " + documentNumber,
                                state:'webForm.tab.orderAckDetails',
                                param:{'msgID': msgID, 'name':"orderAckDetails" + documentNumber}});
        break;
      default:
        break;
    }
  }//End of stateGo function

  $scope.load = function() {
    $scope.loadStuff();
    timeout();
  };/*End of Load Function*/

});
