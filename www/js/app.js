// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

//Custom validation function to check if two input box identical
var compareTo = function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
};

function saveTextAsFile (data, filename){

       if(!data) {
           console.error('Console.save: No data')
           return;
       }

       if(!filename) filename = 'console.json'

       var blob = new Blob([data], {type: 'text/plain'}),
           e    = document.createEvent('MouseEvents'),
           a    = document.createElement('a')
// FOR IE:

 if (window.navigator && window.navigator.msSaveOrOpenBlob) {
     window.navigator.msSaveOrOpenBlob(blob, filename);
 }
 else{
     var e = document.createEvent('MouseEvents'),
         a = document.createElement('a');

     a.download = filename;
     a.href = window.URL.createObjectURL(blob);
     a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':');
     e.initEvent('click', true, false, window,
         0, 0, 0, 0, 0, false, false, false, false, 0, null);
     a.dispatchEvent(e);
 }
}

var app = angular.module('lasersensor', ['ionic', 'angular-md5', 'ngMaterial', 'ui.grid', 'ui.grid.selection', 'ui.grid.cellNav', 'ui.grid.edit', 'ui.grid.expandable', 'ui.grid.pinning', 'ui.grid.grouping', 'ui.bootstrap', 'md.data.table', 'pascalprecht.translate', 'ngMessages', 'moment-picker'])


  //Store the server address into Constant.
  .constant('baseURL', 'http://46.101.83.204:5080')
  .constant('loginURL', 'https://46.101.83.204:5333')

  .constant('localURL', 'http://127.0.0.1:5080')
  .constant('localLoginURL', 'https://127.0.0.1:5333')


  //.run(function($state, $rootScope, $ionicLoading, $ionicPlatform, $ionicPopup, $http, $window, $location, baseURL, localURL, loginURL, localLoginURL) {
  .run(function($state, $rootScope, $ionicLoading, $ionicPlatform, $ionicPopup, $http, $window, $filter, $location, $translate, baseURL, localURL, loginURL, localLoginURL, copyDocumentService, dialogService, httpService){

    $translate.use(window.localStorage.getItem("lang"));
    $rootScope.reloadLangFunctions = [];
    $rootScope.baseURL = window.localStorage.getItem("baseURL");
    $rootScope.mobile = window.localStorage.getItem('mobile')=='1'?true:false;

	//start up coding (required <div ng-init="load()"> in html)
    $rootScope.regLoad = function(){
      console.log("App start");
      //Store the server address into localStorage, this make one place to modify the server address in the future.
      var localhost = false;
      if(localhost){
        window.localStorage.setItem("baseURL", localURL);
        window.localStorage.setItem("loginURL", localLoginURL);
      }else{
        window.localStorage.setItem("baseURL", baseURL);
        window.localStorage.setItem("loginURL", loginURL);
      }//End of localhost if-condition

      var httpURL = window.localStorage.getItem("baseURL");
      var httpsURL = window.localStorage.getItem("loginURL");
      $rootScope.baseURL = httpURL;
      $rootScope.loginURL = httpsURL;

      //If this is not mobile device, force to login screen.
      if(!ionic.Platform.isIOS() && !ionic.Platform.isAndroid() && !ionic.Platform.isIPad()){
        $state.go('lasersensor.tab.main');
        return;
      }else{
        $state.go('lasersensor.tab.main');
        return;
      }
    };//End of reload function

    $rootScope.$on('$translateChangeSuccess', function(event, data) {
      var language = data.language;

      $rootScope.lang = language;
    });

    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

    });
  })

  .config(function($mdThemingProvider) {
    $mdThemingProvider.definePalette('main',{
      '50': 'E8F4FE',
      '100': 'C6E4FC',
      '200': 'A1D2FA',
      '300': '185b95',// Main Colour 185b95 vv
      '400': '5EB3F7',
      '500': '004b8b',//Text lightlights  004b8b^^
      '600': '185b95',
      '700': '3393F2',
      '800': '028090',//264a60
      '900': '1D79EE',
      'A100': 'ff8a80',
      'A200': '032d63',//checkboxes
      'A400': 'ff1744',
      'A700': '032d63',//tab highlights
      'contrastDefaultColor': 'light'
    });
    $mdThemingProvider.theme('default').primaryPalette('main').accentPalette('main');
  });

app.directive('compareTo', compareTo);

app.directive('documentHistory', function(){
  return {
    templateUrl: 'templates/document-history.html'
  }
})

app.directive('vatBreakdown', function(){
  return {
    templateUrl: 'templates/VAT-breakdown.html'
  }
})

//Date format using moment.js
app.config(function ($mdDateLocaleProvider) {
  $mdDateLocaleProvider.formatDate = function (date) {
    return date ? moment(date).format('YYYY-MM-DD') : null;
  };

  $mdDateLocaleProvider.parseDate = function (dateString) {
    var m = moment(dateString, 'YYYY-MM-DD', true);
    return m.isValid() ? m.toDate() : new Date(NaN);
  };
});

// helper functions
function roundUp(value){
  result = Math.ceil(Number((value * 100).toFixed())) / 100;
  return result;
}
